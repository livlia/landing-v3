/* Navbar */
let scrollpos = window.scrollY
const header = document.getElementById("li-navbar")
const header_height = header.offsetHeight

const add_class_on_scroll = () => header.classList.add("active-class")
const remove_class_on_scroll = () => header.classList.remove("active-class")

window.addEventListener('scroll', function () {
    scrollpos = window.scrollY;
    if (scrollpos >= header_height) { add_class_on_scroll() }
    else { remove_class_on_scroll() }
})

const myCollapsible = document.getElementById('navbarSupportedContent');
const navbar = document.getElementById('li-navbar');
myCollapsible.addEventListener('hiden.bs.collapse', ()=> {
    navbar.classList.remove('li-navbar-open')
})
myCollapsible.addEventListener('show.bs.collapse', ()=> {
    navbar.classList.add('li-navbar-open')
})

